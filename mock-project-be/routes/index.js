const homeRoute = require("./home");

const router = (app) => {
    app.use("/home", homeRoute);
}

module.exports = router;
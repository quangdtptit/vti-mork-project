const mysql = require('mysql');

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123456",
    database: "vti_mork_project"
});

connection.connect(function (err) {
    if (err) {
        console.log("Connect fail ...!");
        throw err;
    }
    console.log("Connected!");
});

module.exports = connection;
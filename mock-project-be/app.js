const express = require('express');
const app = express();
const port = 3000;
const morgan = require('morgan');
const routes = require("./routes");

app.use(morgan('dev'));
app.use(
  express.urlencoded({
    extended: true,
  }),
);

app.use(express.json());

routes(app);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
})
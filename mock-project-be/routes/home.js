const app = require("express");
const router = app.Router();

router.get("/", (req, res) => {
    res.json({
        name: "Hello world"
    })
})

module.exports = router;
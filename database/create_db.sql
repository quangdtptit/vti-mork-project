CREATE DATABASE vti_mork_project;
USE vti_mork_project;

CREATE TABLE shop(
	id INT NOT NULL auto_increment PRIMARY KEY,
	name VARCHAR(255),
	phone VARCHAR(10),
	address VARCHAR(255),
	description VARCHAR(300),
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	modified_date TIMESTAMP,
	modified_by VARCHAR(255)
);

CREATE TABLE category_food(
	id INT NOT NULL auto_increment PRIMARY KEY,
	name VARCHAR(255),
	description VARCHAR(300),
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	modified_date TIMESTAMP,
	modified_by VARCHAR(255)
);

CREATE TABLE food(
	id INT NOT NULL auto_increment PRIMARY KEY,
	name VARCHAR(255),
	price BIGINT,
	img VARCHAR(255),
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	modified_date TIMESTAMP,
	modified_by VARCHAR(255),
	id_shop INT,
	id_category_food INT,
	CONSTRAINT fk_food_shop FOREIGN KEY (id_shop) REFERENCES shop(id),
	CONSTRAINT fk_food_cate FOREIGN KEY (id_category_food) REFERENCES category_food(id)
);

CREATE TABLE course(
	id INT NOT NULL auto_increment PRIMARY KEY,
	name VARCHAR(255),
	description VARCHAR(300),
	status INT,
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	id_shop INT,
	CONSTRAINT fk_course_shop FOREIGN KEY (id_shop) REFERENCES shop(id)
);




CREATE TABLE account(
	id INT NOT NULL auto_increment PRIMARY KEY,
	username VARCHAR(255) NOT NULL UNIQUE,
	full_name VARCHAR(255),
	email VARCHAR(255),
	password_hash VARCHAR(255) NOT NULL,
	created_date TIMESTAMP,
	created_by VARCHAR(255)
);


CREATE TABLE role(
	id INT NOT NULL auto_increment PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	code VARCHAR(255) NOT NULL,
	description VARCHAR(300),
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	modified_date TIMESTAMP,
	modified_by VARCHAR(255)
);

CREATE TABLE account_role(
	id_account INT,
	id_role INT,
	PRIMARY KEY(id_account, id_role),
	CONSTRAINT fk_ar_acc FOREIGN KEY (id_account) REFERENCES account(id),
	CONSTRAINT fk_ar_role FOREIGN KEY (id_role) REFERENCES role(id)
)

CREATE TABLE `order`(
	id INT NOT NULL auto_increment PRIMARY KEY,
	amount BIGINT,
	totalItem INT,
	created_date TIMESTAMP,
	created_by VARCHAR(255),
	modified_date TIMESTAMP,
	modified_by VARCHAR(255),
	id_course INT,
	id_account INT,
	CONSTRAINT fk_order_course FOREIGN KEY (id_course) REFERENCES course(id),
	CONSTRAINT fk_order_account FOREIGN KEY (id_account) REFERENCES account(id)
);

CREATE TABLE `order_details`(
	id INT NOT NULL auto_increment PRIMARY KEY,
	number INT,
	price INT,
	id_order INT,
	id_food INT,
	CONSTRAINT fk_od_order FOREIGN KEY (id_order) REFERENCES `order`(id),
	CONSTRAINT fk_od_food FOREIGN KEY (id_food) REFERENCES food(id)
)
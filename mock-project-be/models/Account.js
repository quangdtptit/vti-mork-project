const connection = require("./../config/db");


// Contructer full param
const Account = function () {

}


Account.findAll = async () => {
    const sql = "SELECT * FROM account";
    return await selectAll(sql);
}

Account.findByUsername = async username => {
    const sql = "SELECT * FROM account WHERE username = ?";
    return await selectByUsername(sql, [username]);
}

Account.create = async account => {
    const sql = "";
    return await "";
}

const update = sql => {
    
}

const insert = sql => {

}

const selectAll = sql => {
    const promise = new Promise((resolve, reject) => {
        connection.query(sql, (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        })
        connection.end();
    })

    return promise
        .then(res => res)
        .catch(err => err);
}

const selectByUsername = (sql, params) => {
    const promise = new Promise((resolve, reject) => {
        connection.query(sql, params, (err, res) => {
            if (err)
                reject(err);
            resolve(res[0] === undefined ? {} : res[0]);
        })
        connection.end();
    })
    return promise
        .then(res => res)
        .catch(err => err);
}

async function test() {

}

test();